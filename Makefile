CC=aarch64-linux-gnu-gcc
CFLAGS=-Wall -O2 -fno-strict-aliasing -g -ffreestanding
LDFLAGS=-static -nostdlib
TARGET=qrwlock
HDRS=kernel.h compiler.h rmem.h qrwlock.h
OBJS=main.o qrwlock.o

$(TARGET) : $(OBJS) $(HDRS)
all: $(TARGET)

clean:
	rm -f $(TARGET) $(OBJS) tags

tags:
	ctags -R .

.PHONY: clean all tags
