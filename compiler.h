#ifndef __COMPILER_H
#define __COMPILER_H

#define barrier()	asm volatile("" ::: "memory")
#define __branch_check__(x, expect, is_constant) ({			\
			int ______r;					\
			______r = __builtin_expect(!!(x), expect);	\
			______r;					\
		})

#define likely(x)	(__branch_check__(x, 1, __builtin_constant_p(x)))
#define unlikely(x)	(__branch_check__(x, 0, __builtin_constant_p(x)))
#define __force
#define __aligned(x)	__attribute__((aligned(x)))
#define __always_inline	inline __attribute__((always_inline))
#define noinline	__attribute__((noinline))
#define __noreturn	__attribute__((noreturn))
#define _THIS_IP_	({ __label__ __here; __here: (unsigned long)&&__here; })

#endif /* __COMPILER_H */
