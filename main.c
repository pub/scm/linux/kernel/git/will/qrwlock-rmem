#include "qrwlock.h"

unsigned long __rmem_stop_addr = 0x0;
struct qrwlock lock = __ARCH_RW_LOCK_UNLOCKED;
static int count = 0;

/*
 * Test driver for qrwlock.
 * Want to ensure that writers are serialised wrt everybody else.
 */
static void writer(void)
{
	arch_write_lock(&lock);
	count++;
	barrier();
	count++;
	arch_write_unlock(&lock);
}

static void reader(void)
{
	arch_read_lock(&lock);
	assert((count & 1) == 0);
	arch_read_unlock(&lock);
}

void _start(void)
{
	__rmem_thread_start(writer);
	__rmem_thread_start(writer);
	__rmem_thread_start(reader);
	reader();
}
