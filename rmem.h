#ifndef __RMEM_H
#define __RMEM_H

#include "compiler.h"

extern unsigned long __rmem_stop_addr;

static inline void __noreturn __rmem_stop(unsigned long pc)
{
	__rmem_stop_addr = pc;

	while (1)
		asm volatile(".inst	0xd50bb001");
}

static inline void __rmem_thread_start(void *fn)
{
	register void *x0 asm("x0") = fn;
	asm volatile(".inst	0xd50bb003" : "+r" (x0) :: "memory");
}

#endif /* __RMEM_H */
