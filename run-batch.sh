#!/bin/sh

RMEM=./main.native
export CAMOMILE_DIR=/usr/share/camomile

if [ $# -lt 2 ]; then
	echo "Usage: $0 <binary> <num_threads>"
	exit 1
fi

if ! [ -x ${RMEM} ]; then
	echo "rmem binary (${RMEM}) not found"
	exit 1
fi

${RMEM} -model flat -shallow_embedding true -eager true -eager_local_mem true -hash_prune true -interactive false -loop_limit 1 -elf_threads $2 $1 -isa_defs_path ./ -model forbid_tree_speculation -prune_restarts true -prune_discards true -priority_reduction true -v -allow_partial true
